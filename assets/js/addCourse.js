//Select the form first.
let formSubmit = document.querySelector('#createCourse');


//add event listener
formSubmit.addEventListener("submit",(e)=>{

	//Q: what does preventDefault() do?
	//A: It prevents the normal behaviour of an event. In this case, the venet submit and its default behavious is to referesh the page from submitting

	e.preventDefault()

	//get the values of your input:

	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	// Common error:
	//Uncaught TypeError: Cannot read property 'value' of null
    // 'null'this is the object
    //object.name
    //cannot read property name of null
    //object is null

    //Get the JWT from our localStorage
    let token = localStorage.getItem("token")
    console.log(token);

    //Create a fetch request to add a new course:
    fetch('https://boiling-falls-47982.herokuapp.com/api/courses',{

    	method: 'POST',
    	headers: {
    		'Content-Type' : 'application/json',
    		'Authorization' : `Bearer ${token}`
    	},
    	body: JSON.stringify({
    		name: courseName,
    		description: description,
    		price: price
    	})

    })

    .then(res => res.json())
    .then(data => {

    	//if the creation of the course is successful, redirect admin to the courses page

    	if(data === true){

    		//redirect the admin to the courses page
            alert("New Course Added Successfully")
    		window.location.replace('./courses.html')
    	}else{

    		alert("Course creation Failed. Something went wrong")
    	}
    })

	

})