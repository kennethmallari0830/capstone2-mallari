//course.html?courseId=601cd09542d63415fc020890
//URL Query parameters

/*
   ?
   courseId = (parameter name)
   601cd09542d63415fc020890 = value
*/
//returns the query string in the URL, when console logged>> course.js:10 ?courseId=601cd09542d63415fc020890
// console.log(window.location.search)

//instantiate or creae a new URLSearchParams object.
//This object, URLSearchParams, is used as an interface to gain access tp methods that allows us to specific parts of the query string.

let params = new URLSearchParams(window.location.search)

//The has method for URLSearchParams checks if the courseId key exists in out URL Query string; courseId ay yung params sa URLQuerystring; Pangcheck lang yung method na ito. 
// console.log(params.has('courseId'))

//The get method for the URLSearchParams returns the value of the key passed in as an argumant.

// console.log(params.get('courseId'));

// store the courseID from the URL Query string in a variable:

let courseId = params.get('courseId')

// get the token from localStorage
let token = localStorage.getItem("token");

//Edit Feb 14, this is to get the Admin status and later use if for condition
let adminUser = localStorage.getItem("isAdmin") === "true"
 console.log(adminUser)
 //End of Edit

/*
    undefined = variable was declared without initial value
    not defined = the variable does not exist
*/

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")
let backContainer = document.querySelector("#backContainer")
let enrolleeContainer = document.querySelector("#enrolleeContainer")


//Create a condition for ADMIN user where they will see the users enrolled on the course.


if (adminUser === true) {
	fetch(`https://boiling-falls-47982.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		console.log(data)
		courseName.innerHTML = data.name
		courseDesc.innerHTML = data.description
		coursePrice.innerHTML = data.price
		backContainer.innerHTML = 
		`<a href="./courses.html"  class="btn btn-dark text-white btn-block viewButton my-3">
								Go Back to Courses Lists
							</a>`



		// console.log(data.price)


		if (data.enrollees.length === 0) {
			enrollContainer.innerHTML = `<h5 class="card-title">No Enrollees Available</h5>`

		} else {
			data.enrollees.forEach(enrollee => {
				console.log(enrollee)
				console.log(enrollee.enrolledOn)
			
				let userId = enrollee.userId

				fetch(`https://boiling-falls-47982.herokuapp.com/api/users/details/${userId}`)
				.then(res => res.json())
				.then(data => {


					let enrolleeName = `${data.firstName} ${data.lastName}`;

					let bago = new Date();
					bago= new Date(enrollee.enrolledOn);
					bago1=bago.toLocaleString();
					console.log(bago1)

					if (data) {

						enrolleeContainer.innerHTML=
						`

							<h4>Enrollees:</h4>
						`
						
						enrollContainer.innerHTML +=
							` 
								<div class="card my-4">
									<div class="card-body">										
										<h5 class="card-title">${enrolleeName}</h5>
										 Enrolled On: ${bago1}</p>


										
				
									</div>
								</div>
							`
					} else {
						alert("Something went wrong");
					}
				})
			})
		}

	});
		
}else{
	//Get the details of the single course
//api/courses/:id
//walang option dahil wala ka need ipasa

//get course id sa routes
fetch(`https://boiling-falls-47982.herokuapp.com/api/courses/${courseId}`)
.then(res=> res.json())
.then(data =>{
	console.log(data)
	//pinalitan yung value ni courseName
	// sa data.name, etc, refer to your schema (models)
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-dark">Enroll</button>`
	//One time lang gagamitin si enrollButton na lalagyan  ng event kaya no need to save it in a variable
	// Bakit wala ng e and prevent default? Kasi click event siya, di nag rerefersh sa page unlike yung submit 

	document.querySelector("#enrollButton").addEventListener("click",()=>{

		//add fetch request to enroll our user
		fetch('https://boiling-falls-47982.herokuapp.com/api/users/enroll',{
			method: 'POST',
			headers: {
				//content type, may ipapasang body
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			//redirect the user to the courses page after enrolleing
			

			if(data === true ){

				alert('Thank you for enrolling to the course')
				window.location.replace('./courses.html')
			}else{

					alert("Please Register or Log in")
					window.location.replace('./register.html')
			}
		})
		.catch((err) => {
            alert("You are already enrolled to this Course");
            window.location.replace('./courses.html')
          });

	})
})

}





//Removed: <p class="card-text text-center">Enrolled On: ${enrollee.enrolledOn}</p> below: <h5 class="card-title">${enrolleeName}</h5>





























	
	