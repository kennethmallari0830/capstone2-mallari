let token = localStorage.getItem("token");

//When you get something from localStorage it will be a String. Then we performed below to make it a Boolean
let adminUser = localStorage.getItem("isAdmin") === "true";
let addButton = document.querySelector("#adminButton");

//This IF statememt will allow us to show a button for adding a course  / a button to redirect us to the addCourse page if the user is admin however, if he/she is a guest or regular user they should not see a button
if(adminUser === false || adminUser === null){

	addButton.innerHTML = null

}else{

	addButton.innerHTML=`

		<div class="col-md-2 offset-md-10">
		<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
		</div>
	`
}


//Activity about fetch 02/10/21


				fetch('http://localhost:8000/api/courses/')

				.then(res => res.json())
				.then(data => {

					console.log(data)
				})

//End of Activity
					

