
console.log(window.location.search)


let params = new URLSearchParams(window.location.search);

// spread the values to sey the key-value pairs of the object URLSearchParams
// console.log(...courseId);

// the has method checks if the courseId key exists in the URL query string
// true means that the key exists
console.log(params.has('userId'))

// get method returns the value of the key passed in as an argument
// console.log(params.get('courseId'))

let userId = params.get('userId')
console.log(userId);

let firstName = document.querySelector("#firstName")
let lastName = document.querySelector("#lastName")
let mobileNumber = document.querySelector("#mobileNumber")
let email = document.querySelector("#userEmail")
//let pwd1 = document.querySelector('#password1')
//let pwd2 = document.querySelector('#password2')
//let password; // to store the value of password
let changePasswordButton= document.querySelector("#editPassword");


changePasswordButton.innerHTML = 
        `
                    <div>
                        <a href="./editUserCredentials.html?userId=${userId}" value="${userId}" class="btn btn-block btn-secondary">Change Password</a>
                    </div>


        `

fetch(`https://boiling-falls-47982.herokuapp.com/api/users/details/${userId}`)
.then(res => res.json())
.then(data => {

	console.log(data)

	// assign the current values as placeholders
	firstName.placeholder = data.firstName
	lastName.placeholder = data.lastName
	mobileNumber.placeholder = data.mobileNo
    email.placeholder = data.email 

    firstName.value = data.firstName
    lastName.value = data.lastName
    mobileNumber.value = data.mobileNo
    email.value = data.email 


    document.querySelector("#editUser").addEventListener("submit", (e) => {

    e.preventDefault()

    let token = localStorage.getItem('token')

    let first = firstName.value
    let last = lastName.value
    let mobileNo = mobileNumber.value
    let uemail = email.value


    // if(pwd1.value === '' && pwd2.value === ''){

    //     password =data.password

    // }else if(pwd1.value === pwd2.value){

    //     password = pwd1.value

    // }


    

    fetch('https://boiling-falls-47982.herokuapp.com/api/users', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            id: userId,
            firstName: first,
            lastName: last,
            email: uemail,
            mobileNo: mobileNo
        })
    })
    .then(res => res.json())
    .then(data => {
        
        console.log(data)

        //creation of new course successful
        if(data === true){
            //redirect to courses index page
            alert("Edit Successful");
            window.location.replace("./profile.html")
        }else{
            //error in creating course, redirect to error page
            alert("something went wrong")
        }

    })

})
	

})



