let navItems = document.querySelector("#navSession");
let userToken = localStorage.getItem("token");
let registerLink = document.querySelector("#register");
let isAdmin = localStorage.getItem("isAdmin") === "true";
let profileLink = document.querySelector("#profile")


if(!userToken){

	navItems.innerHTML =

	`
		<li class="nav-item ">
					<a href="./pages/login.html" class="nav-link"> Login </a>
				</li>
 
	`

	registerLink.innerHTML=
		`
		  <li class="nav-item">
		  	<a href="./pages/register.html" class="nav-link">Register</a>	
		  </li>

		`
}else if((userToken) && (isAdmin===false)){
		


	profileLink.innerHTML =

	`
		<li class="nav-item ">
					<a href="./pages/profile.html" class="nav-link"> Profile </a>
				</li>


	`

		navItems.innerHTML =

	`
		<li class="nav-item ">
					<a href="./pages/logout.html" class="nav-link"> Logout </a>
				</li>


	`


}else{

	navItems.innerHTML =

	`
		<li class="nav-item ">
					<a href="./pages/logout.html" class="nav-link"> Logout </a>
				</li>

	`
}












/* Old code

let navItems = document.querySelector("#navSession");
let userToken = localStorage.getItem("token");
let registerLink = document.querySelector("#register")

if(!userToken){

	navItems.innerHTML =

	`
		<li class="nav-item ">
					<a href="./pages/login.html" class="nav-link"> Login </a>
				</li>
 
	`

	registerLink.innerHTML=
		`
		  <li class="nav-item">
		  	<a href="./pages/register.html" class="nav-link">Register</a>	
		  </li>

		`
}else{

	navItems.innerHTML =

	`
		<li class="nav-item ">
					<a href="./pages/logout.html" class="nav-link"> Logout </a>
				</li>

	`
}

*/