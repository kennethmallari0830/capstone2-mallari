//Run locally: http://localhost:8000/

let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e) => {
  e.preventDefault();

  let email = document.querySelector("#userEmail").value;
  let password = document.querySelector("#password").value;

  console.log(email);
  console.log(password);

  if (email == "" || password == "") {
    alert("Please input your Email and/or password.");
  } else {
    fetch("https://boiling-falls-47982.herokuapp.com/api/users/login", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        //add if-else statement which will verify our users that they have logged in successfully and got their tokens. If the user cannot log in successfully, they will get an alert. If the user has logged in successfuly, we will save his token into the web browser and  get his details using fetch. To save data to a web browser, we access the local storage. The localStorage is used to save data into the web browser. This storage is read only, therefore we set our data into localStorage through JS by providing a key/value pair into the storage.

        //the setItem() for localstorage allows us to save data into the localStorage. This is the syntax: localStorage.setItem("key",data).

        //To get an item from a localStorage, this is the syntax: localStorage.getItem("key")

        if (data.accessToken) {
          //Set the token into the local storage
          localStorage.setItem("token", data.accessToken);

          //Send a fetch request to decode the JWT and obtain the userID and isAdmin property.

          fetch("https://boiling-falls-47982.herokuapp.com/api/users/details", {
            //kapag GET, kahit wala ng method : 'GET'

            headers: {
              Authorization: `Bearer ${data.accessToken}`,
            },
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);
              localStorage.setItem("id", data._id);
              localStorage.setItem("isAdmin", data.isAdmin);

              //window.location.replace= this allows us to redirect our user to another page.
              window.location.replace("./courses.html");
            });
        } else {
          alert("Logged in failed. Something went wrong");
        }
      });
  }
});
