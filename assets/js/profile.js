let name = document.querySelector("#userName");
let desc = document.querySelector("#userDesc");
let userEmail = document.querySelector("#userEmail");
let userCourses = document.querySelector("#enrollContainer");
let token = localStorage.getItem("token");
let courseArray = [];
let editButton = document.querySelector("#editButton");
let backContainer = document.querySelector("#backContainer");

// console.log(name,desc,userCourses);

fetch(`https://boiling-falls-47982.herokuapp.com/api/users/details`, {
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((res) => res.json())
  .then((data) => {
    // console.log(data)
    // console.log(data.enrollments[0].courseId)
    // console.log(data.enrollments.length)
    if (data) {
      name.innerHTML = ` ${data.firstName}  ${data.lastName}`;
      desc.innerHTML = `Mobile Number: ${data.mobileNo}`;
      userEmail.innerHTML = `Email: ${data.email}`;
      editButton.innerHTML = `

			<div class="col-md-2 offset-md-10">
				<a href="./editProfile.html?userId=${data._id}" value="${data._id}" class="btn btn-block btn-secondary">Edit</a>
			</div>
		`;
    } else {
      alert("Something went wrong");
    }

    data.enrollments.forEach((course) => {
      console.log(course);
    });

    data.enrollments.forEach((course) => {
      // for (let i = 0; i < data.enrollments.length; i++) {

      let courseId = course.courseId;

      fetch(
        `https://boiling-falls-47982.herokuapp.com/api/courses/${courseId}`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        }
      )
        .then((res) => res.json())
        .then((data) => {
          // console.log(data.name)
          let bago = new Date();
          bago = new Date(course.enrolledOn);
          bago1 = bago.toLocaleString();
          console.log(bago1);

          if (data) {
            userCourses.innerHTML += ` 
					<div class="card my-5">
						<div class="card-body">
							<h5 class="card-title">${data.name}</h5>
							<p class="card-text">Date Enrolled: ${bago1}</p>
							<p class="card-text"> Status: ${course.status}</p>


						</div>
					</div>
				`;
            // courseArray.push(data.name)
            // console.log(courseArray)
          } else {
            alert("Something went wrong");
          }
          // courseArray.forEach(courseName => console.log(courseName));
        });
    });
  });
