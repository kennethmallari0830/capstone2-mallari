
let registerForm = document.querySelector("#registerUser");

/*

console.log(firstName)
console.log(lastName)
console.log(mobileNo)
console.log(email)
console.log(password1)
console.log(password2)

*/
//addEventListener("event",function)

/*
   Allows us to add a specific event into an elemnt. This event can trigger a function for our page

*/
// Submit event allows us to submit a form.
// It's default behaviour

registerForm.addEventListener("submit",(e)=>{

	// e is our event object. This event object pertains to the event and where it was triggerred

	//preventDefault() = prevents the submit event of its default behavior

	e.preventDefault()

	console.log("I triggered the submit event")
	//most elements that has .value are inputs
	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value;
	let mobileNo = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	if((password1 !== '' && password2 !== '') &&(password1 === password2) && (mobileNo.length === 11)){
    
    	//fetch(url,options) '/' GET request; if you are sending info, POST request siya

    		fetch('https://boiling-falls-47982.herokuapp.com/api/users/email-exists',{
    			method: 'POST',
    			headers: { 'Content-Type': 'application/json' },
    			body: JSON.stringify({
    				email : email
    			})
    		})
    		.then(res => res.json())
    		.then(data=>{
    			console.log(data)

    			if (data=== false){

					fetch('https://boiling-falls-47982.herokuapp.com/api/users',{
						method: 'POST',
						//kapag POST matic na yung may {Content-Type}
						headers: {'Content-Type': 'application/json'},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							password: password1,
							mobileNo: mobileNo 

						})
					})
					.then(res => res.json())
					.then(data => {

						console.log(data);

						if(data===true){
							alert("Registered sucessfully")

							//redirects to the log in page
							window.location.replace("./login.html")
						}else{
							//error in creating registration
							alert("something went wrong")

						}
					})

				}else{
							//error in creating registration
							alert("Email Exists")

						}
    		})
		
	}else{
		
		alert('Please fill out the form properly')
		 
	}

	

})


// pag papunta sa server, convert to string, pag palabas ng server, convert to json

